import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Notes from "./components/Notes";
import Tags from "./components/Tags";
import Header from "./components/Header";
import Footer from "./components/Footer";
import "./App.css";

window.baseUrl = "http://localhost/backend";

function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/">
          <Notes />
        </Route>
        <Route path="/tags">
          <Tags />
        </Route>
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
