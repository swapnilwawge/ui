import React, { useEffect, useState } from 'react'
import AddTag from './AddTag'

const Tags = () => {

  const [tags, setTags] = useState([])
  const [message, setMessage] = useState()

  useEffect(() => {
    const fetchTags = async () => {
      try {
        const response = await fetch(`${window.baseUrl}/tags/getTags`);
        const { responseCode, result } = await response.json();
        if(responseCode == 200) {
          setTags(result)
        }
      } catch (error) {
        console.log("error", error);
      }
    }

    fetchTags()
  }, [])

  const handleDeleteTag = (e, tag) => {
    e.preventDefault()
    fetch(`${window.baseUrl}/tags/deleteTag`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({id: tag.id})
    }).then(response => response.json())
    .then(response => {
      const { responseCode, message } = response
      if(responseCode == 200) {
        setTags([...tags.filter((item) => item != tag)])
        setMessage(message)
        setTimeout(() => setMessage(''), 5000)
      }
    })
  }

  const handleAddTag = (tag) => {
    setTags([...tags, tag])
    setMessage('Tag added successfully')
    setTimeout(() => setMessage(''), 5000)
  }

  return (
    <div className="container py-5">
      <AddTag handleAddTag={handleAddTag} />
      {!!message && (
        <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
          {message}
        </div>
      )}
      <div className="row mt-5">
        {tags.length > 0
        ? <>
          {tags.map((tag, key) => (
            <div className="row" key={key}>
              <div className="col-sm-3">{tag.title}</div>
              <div className="col">
                <a href="#" onClick={(e) => handleDeleteTag(e, tag)} className="text-danger">delete</a>
              </div>
            </div>
          ))}
        </>
        : <p className="p-0">No Tags Available</p>
        }
      </div>
    </div>
  )
}

export default Tags;