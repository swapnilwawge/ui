import React, { useEffect, useState } from "react";

const AddNote = ({ handleAddNote }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [tags, setTags] = useState([]);
  const [message, setMessage] = useState("");
  const [availableTags, setAvailableTags] = useState([]);

  useEffect(() => {
    const fetchTags = async () => {
      try {
        const response = await fetch(`${window.baseUrl}/tags/getTags`);
        const { responseCode, result } = await response.json();
        if (responseCode == 200) {
          setAvailableTags(result);
        }
      } catch (error) {
        console.log("error", error);
      }
    };

    fetchTags();
  }, []);

  const handleChange = ({ checked, value }) => {
    let selectedTags = tags;
    let selectedTag = parseInt(value);
    if (checked) {
      selectedTags.push(selectedTag);
    } else {
      selectedTags = selectedTags.filter((item) => item != selectedTag);
    }

    setTags([...selectedTags]);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (title && description && tags.length > 0) {
      fetch(`${window.baseUrl}/notes/addNote`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          title: title,
          description: description,
          tags: tags,
        }),
      })
        .then((response) => response.json())
        .then((response) => {
          const { responseCode, result } = response;
          if (responseCode == 200) {
            setMessage("Note added successfully");
            setTitle("");
            setDescription("");
            setTags([]);
            handleAddNote(result);
            setTimeout(() => setMessage(""), 5000);
          }
        });
    } else {
      alert("Title, description or tags are missing");
    }
  };

  return (
    <div
      className="modal fade"
      id="addNoteModalCenteredScrollable"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabIndex="-1"
      aria-labelledby="addNoteModalCenteredScrollableLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div className="modal-content">
          <div className="modal-header">
            <h5
              className="modal-title"
              id="addNoteModalCenteredScrollableLabel"
            >
              Add Note
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            {!!message && (
              <div class="alert alert-success" role="alert">
                {message}
              </div>
            )}
            <form onSubmit={(e) => e.preventDefault()}>
              <div className="mb-3">
                <label htmlFor="recipient-name" className="col-form-label">
                  Title:
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="recipient-name"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="message-text" className="col-form-label">
                  Description:
                </label>
                <textarea
                  className="form-control"
                  id="message-text"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                ></textarea>
              </div>
              <div className="mb-3">
                <label htmlFor="message-text" className="col-form-label">
                  Tags:
                </label>
                {availableTags.map((tag, key) => {
                  return (
                    <div className="form-check" key={key}>
                      <input
                        className="form-check-input"
                        type="checkbox"
                        name="flexCheckDefault[]"
                        value={tag.id}
                        onChange={(e) => handleChange(e.target)}
                        id={`flexCheckDefault${tag.id}`}
                      />
                      <label
                        className="form-check-label"
                        htmlFor={`flexCheckDefault${tag.id}`}
                      >
                        {tag.title}
                      </label>
                    </div>
                  );
                })}
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Cancel
            </button>
            <button
              type="button"
              onClick={handleSubmit}
              className="btn btn-primary"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddNote;
