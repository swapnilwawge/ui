import React, { useEffect, useState } from 'react'
import AddNote from './AddNote'

const Notes = () => {

  const [notes, setNotes] = useState([])

  const handleAddNote = (note) => {
    let allNotes = notes
    allNotes.unshift(note)
    setNotes([...allNotes])
  }

  useEffect(() => {
    const fetchNotes = async () => {
      try {
          const response = await fetch(`${window.baseUrl}/notes/getNotes`);
          const { responseCode, result } = await response.json();
          if(responseCode == 200 ) {
            setNotes(result)
          }
      } catch (error) {
          console.log("error", error);
      }
    }

    fetchNotes()
  }, [])

  const StyleSpan = {
    float: 'right'
  }

  return (
    <div className="container py-5">
       <div className="row align-items-start">
        <div className="col p-0">
          <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addNoteModalCenteredScrollable">Add Note</button>
        </div>
      </div>
      <div className="row mt-2">
        {notes.length > 0
        ? <>
          {notes.map((note, key) => {
            return (
              <div className="card mt-2" key={key}>
                <div className="card-body">
                  <h5 className="card-title">
                    <span>{note.title}</span>
                    <span style={StyleSpan}>Created: {note.dateTime}</span>
                  </h5>
                  <p className="card-text">{note.description}</p>
                  {note.tags.length > 0 && note.tags.map((tag) => {
                    return (
                      <button key={tag} href="#" className="btn btn-light m-1">{tag}</button>
                    )
                  })}
                </div>
              </div>
            )
          })}
        </>
        : <p className="p-0">No Notes Available</p>}
      </div>

      <AddNote handleAddNote={handleAddNote} />
    </div>
  )
}

export default Notes;