import React from 'react'

const Footer = () => (
  <footer className="bd-footer py-5 bg-light">
    <p>©2021 Notes, Inc.</p>
  </footer>
)

export default Footer
