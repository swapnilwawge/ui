import React, { useState } from 'react'

const AddTag = ({handleAddTag}) => {

  const [title, setTitle] = useState('')

  const handleAdd = () => {
    if(title) {
      fetch(`${window.baseUrl}/tags/addTag`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({title: title})
      }).then(response => response.json())
      .then(response => {
        const { responseCode, result } = response
        if(responseCode == 200) {
          setTitle('')
          handleAddTag(result)
        }
      })
    } else {
      alert('Tag title is required')
    }
  }

  return (
    <div className="row align-items-start g-2">
      <div className="col-sm-7 p-0">
        <input type="text" className="form-control" placeholder="Enter title here" aria-label="add tag" value={title} onChange={(e) => setTitle(e.target.value)} />
      </div>
      <div className="col-sm">
        <button type="button" className="btn btn-primary" onClick={handleAdd}>Add Tag</button>
      </div>
    </div>
  )
}

export default AddTag;